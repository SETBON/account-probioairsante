<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('logged/{id}', 'HomeController@logged');
Route::get('redirect/{group_id}', ["as" => "redirect", "uses" => "HomeController@redirect"]);

Route::get('admin/{id}', 'HomeController@admin');
