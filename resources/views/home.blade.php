<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Inscription - Probioairsante</title>
</head>

<body>
    <nav>
        <div class="nav-wrapper deep-orange darken-1">
            <a href="#" class="brand-logo">
                <img src="https://probioairsante.shop/img/probioairsante-logo-1597340296.jpg" alt="" width="230" class="responsive-img m10">
            </a>
            <div class="nav-content">
                <a onclick="history.back()" class="btn-floating btn-large halfway-fab waves-effect waves-light teal">
                    <i class="material-icons">keyboard_arrow_left</i>
                </a>
            </div>
        </div>
    </nav>

    <div class="row">
        <div class="col s12">
            <div class="card">
                <form id="formCreateCustomer" action="{{ route('store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="type" value="{{ $type }}">
                    <div class="card-content">
                        <h2 class="card-title">Création de votre compte</h2>
                        <div class="card blue white-text">
                            <div class="card-content">
                                <p><i class="material-icons">information</i> Vous êtes sur le point de créer votre compte sur le site probioairsante.shop et d'après le système vous voulez ouvrir un compte de type <strong>{{ $type }}</strong></p>
                            </div>
                        </div>
                        @if($errors->any())
                        <div class="card red white-text">
                            <div class="card-content">
                                <p><i class="material-icons">report_problem</i> Erreur !</p>
                                <ul>
                                    @foreach($errors as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif
                        @if($message = session('warning'))
                            <div class="card orange white-text">
                                <div class="card-content">
                                    <p><i class="material-icons">report_problem</i> {{ $message }}</p>
                                </div>
                            </div>
                        @endif
                        @if($message = session('error'))
                            <div class="card red white-text">
                                <div class="card-content">
                                    <p><i class="material-icons">report_problem</i> {{ $message }}</p>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col s3">&nbsp;</div>
                            <div class="col s6">
                                @if($type == "pro")
                                    <div class="input-field col s12">
                                        <input id="icon_prefix" type="text" class="validate" name="company" required>
                                        <label for="icon_prefix">Nom de votre société</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="icon_prefix" type="text" class="validate" name="siret" required>
                                        <label for="icon_prefix">Numéro de Siret</label>
                                    </div>
                                @endif
                                    @if($type == "collectivite")
                                        <div class="input-field col s12">
                                            <input id="icon_prefix" type="text" class="validate" name="company" required>
                                            <label for="icon_prefix">Identité de votre collectivité, mairie, etc...</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="icon_prefix" type="text" class="validate" name="siret" required>
                                            <label for="icon_prefix">Numéro de Siret</label>
                                        </div>
                                    @endif
                                    @if($type == "sante")
                                        <div class="input-field col s12">
                                            <input id="icon_prefix" type="text" class="validate" name="company" required>
                                            <label for="icon_prefix">Identité</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="icon_prefix" type="text" class="validate" name="siret" required>
                                            <label for="icon_prefix">Numéro de Siret</label>
                                        </div>
                                    @endif
                                <div class="row">
                                    <div class="input-field col s6">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="icon_prefix" type="text" class="validate" name="firstname" required>
                                        <label for="icon_prefix">Nom de famille</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="icon_prefix" type="text" class="validate" name="lastname" required>
                                        <label for="icon_prefix">Prénom</label>
                                    </div>
                                </div>
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">mail</i>
                                    <input id="icon_prefix" type="text" class="validate" name="email" required>
                                    <label for="icon_prefix">Adresse Mail</label>
                                </div>
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">https</i>
                                    <input id="icon_prefix" type="password" class="validate" name="password" required>
                                    <label for="icon_prefix">Mot de passe</label>
                                </div>
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">cake</i>
                                    <input id="birthday" type="date" class="validate" name="birthday">
                                    <label for="birthday"></label>
                                </div>
                                <p class="col s12">
                                    <input type="checkbox" name="optin" id="optin">
                                    <label for="optin">Recevoir les offres de nos partenaires</label>
                                </p>
                                <p class="col s12">
                                    <input type="checkbox" name="newsletter" id="newsletter">
                                    <label for="newsletter">Recevoir notre newsletter</label><br>
                                    <i>Vous pouvez vous désinscrire à tout moment. Vous trouverez pour cela nos informations de contact dans les conditions d'utilisation du site.</i>
                                </p>
                                <br />
                                <p class="col s12">
                                    <input type="checkbox" name="cvg_accept" id="cvg_accept" required>
                                    <label for="cvg_accept">J'accepte <a href="https://probioairsante.shop/content/3-cvg">les conditions générales</a> et <a href="https://probioairsante.shop/content/2-mentions-legales">la politique de confidentialité</a></label>
                                </p>
                            </div>
                            <div class="col s3">&nbsp;</div>
                        </div>
                    </div>
                    <div class="card-action center">
                        <button type="submit" class="btn btn-flat green white-text" id="btnSubmitCustomers">
                            <i class="material-icons">check</i> Valider
                        </button>
                        <div id="buttonReturn">

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.fr.min.js"></script>
<script type="text/javascript">
    (function ($) {
        $("#birthday").datepicker({
            todayBtn: "linked",
            clearBtn: true,
            language: "fr"
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#formCreateCustomer").on('submit', (e) => {
            e.preventDefault()
            let form = $("#formCreateCustomer");
            let url = form.attr('action')
            let data = form.serializeArray()
            let btn = $("#btnSubmitCustomers")

            btn.addClass('disabled')

            $.ajax({
                url: url,
                data: data,
                method: "POST",
                statusCode: {
                    200: (data) => {
                        btn.removeClass('disabled');
                        Materialize.toast("Votre compte à été créer avec succès", 3000, "green-text")
                        console.log(data.type)

                            if(data.type === 'particulier') {
                                $("#buttonReturn").html(`<a href="https://probioairsante.shop/connexion?back=my-account" class="btn blue"><i class="material-icons">keyboard_arrow_left</i> Retour au site</a>`).show()
                            }
                            if(data.type === 'pro') {
                                $("#buttonReturn").html(`<a href="https://probioairsante.shop/connexion?back=my-account" class="btn blue"><i class="material-icons">keyboard_arrow_left</i> Retour au site</a>`).show()
                            }
                            if(data.type === 'collectivite') {
                                $("#buttonReturn").html(`<a href="https://probioairsante.shop/connexion?back=my-account" class="btn blue"><i class="material-icons">keyboard_arrow_left</i> Retour au site</a>`).show()
                            }
                            if(data.type === 'sante') {
                                $("#buttonReturn").html(`<a href="https://probioairsante.shop/connexion?back=my-account" class="btn blue"><i class="material-icons">keyboard_arrow_left</i> Retour au site</a>`).show()
                            }

                    },
                    422: (data) => {
                        btn.removeClass('disabled');
                        Materialize.toast("Erreur de validation !", 3000, "orange-text")
                    },
                    423: (data) => {
                        btn.removeClass('disabled');
                        Materialize.toast(data.errors, 3000, 'orange-text')
                    },
                    500: (jqxhr) => {
                        btn.removeClass('disabled');
                        Materialize.toast(jqxhr.errors, 3000, 'red-text')
                    }
                }
            })


        })
    })(jQuery)
</script>
</body>
</html>
