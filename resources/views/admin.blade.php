<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Selection - Probioairsante</title>
</head>

<body>
    <nav>
        <div class="nav-wrapper deep-orange darken-1">
            <a href="#" class="brand-logo">
                <img src="https://probioairsante.shop/img/probioairsante-logo-1597340296.jpg" alt="" width="230" class="responsive-img m10">
            </a>
            <div class="nav-content">
                <a onclick="history.back()" class="btn-floating btn-large halfway-fab waves-effect waves-light teal">
                    <i class="material-icons">keyboard_arrow_left</i>
                </a>
            </div>
        </div>
    </nav>

    <div class="row">
        <div class="col s12">
            <div class="card text-center">
                <a href="https://pro.probioairsante.shop/connexion?back=my-account&email=test@test.com&password=test123456" class="waves-effect waves-light btn-large">Accès Professionnel</a>
                <a href="https://collectivite.probioairsante.shop/connexion?back=my-account&email=test@test.com&password=test123456" class="waves-effect waves-light btn-large">Accès Collectivité</a>
                <a href="https://sante.probioairsante.shop/connexion?back=my-account&email=test@test.com&password=test123456" class="waves-effect waves-light btn-large">Accès Santé</a>
            </div>
        </div>
    </div>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.fr.min.js"></script>
<script type="text/javascript">
    (function ($) {
        $("#birthday").datepicker({
            todayBtn: "linked",
            clearBtn: true,
            language: "fr"
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#formCreateCustomer").on('submit', (e) => {
            e.preventDefault()
            let form = $("#formCreateCustomer");
            let url = form.attr('action')
            let data = form.serializeArray()
            let btn = $("#btnSubmitCustomers")

            btn.addClass('disabled')

            $.ajax({
                url: url,
                data: data,
                method: "POST",
                statusCode: {
                    200: (data) => {
                        btn.removeClass('disabled');
                        Materialize.toast("Votre compte à été créer avec succès", 3000, "green-text")
                        console.log(data.type)

                            if(data.type === 'particulier') {
                                $("#buttonReturn").html(`<a href="https://probioairsante.shop/connexion?back=my-account" class="btn blue"><i class="material-icons">keyboard_arrow_left</i> Retour au site</a>`).show()
                            }
                            if(data.type === 'pro') {
                                $("#buttonReturn").html(`<a href="https://probioairsante.shop/connexion?back=my-account" class="btn blue"><i class="material-icons">keyboard_arrow_left</i> Retour au site</a>`).show()
                            }
                            if(data.type === 'collectivite') {
                                $("#buttonReturn").html(`<a href="https://probioairsante.shop/connexion?back=my-account" class="btn blue"><i class="material-icons">keyboard_arrow_left</i> Retour au site</a>`).show()
                            }
                            if(data.type === 'sante') {
                                $("#buttonReturn").html(`<a href="https://probioairsante.shop/connexion?back=my-account" class="btn blue"><i class="material-icons">keyboard_arrow_left</i> Retour au site</a>`).show()
                            }

                    },
                    422: (data) => {
                        btn.removeClass('disabled');
                        Materialize.toast("Erreur de validation !", 3000, "orange-text")
                    },
                    423: (data) => {
                        btn.removeClass('disabled');
                        Materialize.toast(data.errors, 3000, 'orange-text')
                    },
                    500: (jqxhr) => {
                        btn.removeClass('disabled');
                        Materialize.toast(jqxhr.errors, 3000, 'red-text')
                    }
                }
            })


        })
    })(jQuery)
</script>
</body>
</html>
