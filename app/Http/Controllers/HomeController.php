<?php

namespace App\Http\Controllers;

use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Protechstudio\PrestashopWebService\Exceptions\PrestashopWebServiceException;
use Protechstudio\PrestashopWebService\PrestashopWebService;

class HomeController extends Controller
{
    /**
     * @var PrestashopWebService
     */
    private $prestashop;

    /**
     * HomeController constructor.
     * @param PrestashopWebService $prestashop
     */
    public function __construct(PrestashopWebService $prestashop)
    {
        $this->prestashop = $prestashop;
    }

    public function register(Request $request)
    {
        /*if(empty($request->type) || $request->type !== "particulier" || $request->type !== "pro" || $request->type !== "collectivite" || $request->type !== "sante")
        {
            header('Location: https://probioairsante.shop');
            exit();
        }*/
        //dd($schema);
        return view("home", ["type" => $request->type]);
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                "type" => "required|string",
                "firstname" => "required|string",
                "lastname" => "required|string",
                "email" => "required|email",
                "password" => "required",
            ]);
        }catch (\Illuminate\Validation\ValidationException $exception) {
            return response()->json($exception->errors(), 422);
        }

        // Vérification si Pro,Santé ou Collectivité
        if ($request->get('type') !== "particulier") {
            $call = $this->call(null, $request->get('siret'));
            if($request->get('type') == "pro") {
                if ($call['etablissement']['uniteLegale']['categorieEntreprise'] !== "PME") {
                    return response()->json(["message" => "Attention", "errors" => "Votre entreprise ne correspond pas au type de compte choisie: <strong>PRO</strong>"], 423);
                }
            }elseif ($request->get('type') == "collectivite") {
                if ($call['etablissement']['uniteLegale']['categorieEntreprise'] !== "ETI") {
                    return response()->json(["message" => "Attention", "errors" => "Votre entreprise ne correspond pas au type de compte choisie: <strong>Collectivité / Mairie</strong>"], 423);
                }
            }elseif($request->get('type') == "collectivite") {
                if ($call['etablissement']['uniteLegale']['categorieEntreprise'] !== "GE") {
                    return response()->json(["message" => "Attention", "errors" => "Votre entreprise ne correspond pas au type de compte choisie: <strong>Professionnel de santé</strong>"], 423);
                }
            }
        }


        // Inscription du client
        try {
            $schema = $this->prestashop->getSchema('customers');
        } catch (\Protechstudio\PrestashopWebService\PrestaShopWebserviceException $e) {
            return response()->json(["message" => "Error", "errors" => $e->getMessage()], 500);
        }
        $password = md5(env("APP_NAME").$request->get('password'));
        if($request->exists('optin') == true){$optin = 1;}else{$optin = 0;}
        if($request->exists('newsletter') == true){$newsletter = 1;}else{$newsletter = 0;}
        if($request->get('birthday') != null){$birthday = Carbon::createFromTimestamp(strtotime(str_replace('/', '-', $request->get('birthday'))))->format('Y-m-d');}else{$birthday = null;}
        if($request->get('type') == "pro" || $request->get('type') == 'sante' || $request->get('type') == "collectivite") {
            switch ($request->get('type')) {
                case 'pro': $group = 5; break;
                case 'collectivite': $group = 6; break;
                case 'sante': $group = 7; break;
                default: $group = 3; break;
            }
            $call = $this->call(null, $request->get('siret'));
            $data = [
                "passwd" => $password,
                "lastname" => $request->get('lastname'),
                "firstname" => $request->get('firstname'),
                "email" => $request->get('email'),
                "birthday" => $birthday,
                "newsletter" => $newsletter,
                "optin" => $optin,
                "company" => $request->get('company'),
                "siret" => $request->get('siret'),
                "ape" => str_replace('.', '', $call['etablissement']['uniteLegale']['activitePrincipaleUniteLegale']),
                "active" => 1,
                "id_default_group" => $group
            ];
        } else {
            $data = [
                "passwd" => $password,
                "lastname" => $request->get('lastname'),
                "firstname" => $request->get('firstname'),
                "email" => $request->get('email'),
                "birthday" => $birthday,
                "newsletter" => $newsletter,
                "optin" => $optin,
                "active" => 1,
                "id_default_group" => 3
            ];
        }

        $postXml = $this->prestashop->fillSchema($schema, $data);
        try {
            $this->prestashop->add(['resource' => "customers", "postXml" => $postXml->asXML()]);
        } catch (PrestashopWebServiceException $e) {
            return response()->json(["message" => "Error", "errors" => $e->getMessage()], 500);
        }

        return response()->json(["type" => $request->get('type')], 200);
    }

    protected function call($siren = null, $siret = null)
    {
        $ch = curl_init();

        if (!empty($siren)) {
            //dd("siren");
            curl_setopt($ch, CURLOPT_URL, 'https://api.insee.fr/entreprises/sirene/V3/siren/' . $siren);
        } else {
            //dd("siret", $siret);
            curl_setopt($ch, CURLOPT_URL, 'https://api.insee.fr/entreprises/sirene/V3/siret/' . $siret);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer 31dc0e58-f46a-3f93-83ca-15e547778a97';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return json_decode($result, true);
    }

    public function redirect($group_id) {
        switch ($group_id) {
            case 3: return response()->json(['link' => "probioairsante.shop"]);
            case 5: return response()->json(['link' => "pro.probioairsante.shop"]);
            case 6: return response()->json(['link' => "collectivite.probioairsante.shop"]);
            case 7: return response()->json(['link' => "sante.probioairsante.shop"]);
            default: return abort(401);
        }
    }

    public function logged($id)
    {
        $schema = $this->prestashop->get([
            'resource' => 'customers',
            'id' => $id
        ]);

        $enc = json_encode($schema);
        $result = json_decode($enc, true);

        return $result['customer']['id_default_group'];
    }

    public function admin($user_id) {
        if($user_id == 4) {
            return response()->json(['link' => "account.probioairsante.shop/admin/4"]);
        } else {
            return abort(401, "Vous n'est pas autorisée !");
        }
    }

    public function adminRedirect($user_id)
    {
        if($user_id == 4) {
            return view('admin');
        } else {
            abort(401, "Vous n'est pas autorisée !");
        }
    }
}
